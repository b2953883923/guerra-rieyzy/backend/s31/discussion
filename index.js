// [SECTION] JSON Objects
	
	/*
		-JSON stands for JavaSCripts Object Notation
		- JSON is also used in other programmin language hench the name JavaScript Object Notation.
		- JSON is used for serializing different data types into bytes. 
			- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information/

		- Uses double qoutes for property names

		Syntax:

		{
			"propertyA": "valueA",
			"popertyB": "valueB"
		}

	*/

// JSON Objects
// {
// 	"city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"country": "Philippines"
// }

// [SECTION] JSON Arrays
// "cities": [
// {"city":  "quezon city", "province" : "Metro Manila", "country": "Philippines"},
// {"city":  "quezon city", "province" : "Metro Manila", "country": "Philippines"},
// {"city":  "quezon city", "province" : "Metro Manila", "country": "Philippines"}
// ];

// [SECTION] JSON Methods
	// The JSON OBjects contain methods for parsing and converting data into stringified JSON

// JavaScript Array of Object.
	let batchesArray = [
	{batchName: "Batch 294"},
	{batchName: "Batch 295"},
	{batchName: "Batch 296"}
	];

	console.log(batchesArray);

// JSON Stringify
// The "stringify" method is used to convert JavaScript Objects into string/JSON

console.log("Result from stringify method:")
console.log(JSON.stringify(batchesArray));

let person = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log("Result from stringify method:")
console.log(person);

// [SECTION] Using Stringify Method with Variables

/*
	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.
	- The "property"  name and "value" wold have the same which can be confusing for beginners.

		- ex: "firstName" : "firstName"
*/ 

// let firstName = prompt('What is your first name?');
// let lastName = prompt('What is your last name?');
// let age = prompt('What is your age?');
// let address = {
// 	city: prompt('Which city do you live in?'),
// 	country: prompt('Which country does your city address belong to?')
// };

// This conversion of JavaScript Object to JSON Data/Object occurs behind the scene
// We usually perform this in our server's controller.
// let anotherPerson = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address : address
// })

// console.log("Result of using JSON method with variables")
// console.log(anotherPerson);

// [SECTION] Converting Stringified JSON into JavaScript Objects

/*
	- Objects are common data types used in applications because of the complex data structures that can be created out of it.
	- Information is commonly sent to application in stringified JSON and then converted back into JavaScript Objects.
		- This is because servers can only read JSON or XML format while browser
		mostly prefer JavaScript Objects.
	- This happens both for sending information to a back app and sending information back to a frontend app.
*/ 

let batchesJSON = `[
{"batchName": "Batch 294"},
{"batchName": "Batch 295"},
{"batchName": "Batch 296"}
]`;
console.log("Result of the JSON parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name" : "Jhon",
	"age" : "31",
	"address" : {
		"city" : "Manila",
		"country": "Philippines"
	}
}`

console.log("Result of the JSON parse method:");
console.log(JSON.parse(stringifiedObject));
